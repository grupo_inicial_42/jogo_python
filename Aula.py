print("Ola Mundo!")

# Linhas comentadas usam # para tal!

# help(print) --> Use caso queira saber mais da função PRINT por exemplo.

# Para escrever MAIS DE UMA LINHA NO SHELL é só usar ;\ no final.
# Exemplo:
# var = 5 ;\
# print("A variável é {}".format(x))
# OUTPUT: A variável é 5

print("Brasil ganhou 5 titulos mundiais!")
print("Brasil", "ganhou", 5, "titulos", "mundiais!", sep = "-")
print("Brasil", "ganhou", 5, "titulos", "mundiais!", sep = "\n")

pais = "Italia"
type(pais) # --> class: str
quantidade = 4
type(quantidade) # --> class: int
print(pais, "ganhou", quantidade, "titulos", "mundiais")

subst = "Python"
verbo = "é"
adjetivo = "fantástico"
print(subst, verbo, adjetivo, sep="_", end="!\n")
# OUTPUT: Python_é_fantástico!

# O Python utiliza por convenção o padrão Snake_Case para nomes de variáveis (ou identificadores).
# Um exemplo de variáveis em Snake_Case são:
idade_esposa = 20
perfil_vip = 'Flávio Almeida'
recibos_em_atraso = 30

# EXEMPLOS DE FORMATAÇÃO DE STRING:
# https://docs.python.org/3/library/string.html#formatexamples

"R$ {:7.1f}".format(1000.12) # Output: R$  1000.1
"R$ {:07.2f}".format(4.11) # Output: 0004.11