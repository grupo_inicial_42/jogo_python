#encoding: utf-8
import random

def jogar_adivinhacao():
    print("*********************************")
    print("Bem vindo no jogo de Adivinhação!")
    print("*********************************")

    numero_secreto = round(random.randrange(1, 101))
    # A função random.randrange() gera um numero aleatório entre 1 até 100.
    # A função round arredonda o resultado SEMPRE PARA BAIXO.
    total_de_tentativas = 0
    pontos = 1000
    # contador = 1

    print("Qual nível de dificuldade?")
    print("(1) Fácil / (2) Médio / (3) Difícil")
    nivel = int(input("Defina o nível: "))
    # input por padrão vem em formato de STRING. Usamos a função int() para transformar para int.

    if(nivel == 1):
        total_de_tentativas = 20
    elif(nivel == 2):
        total_de_tentativas = 10
    else:
        total_de_tentativas = 5

    for rodada in range(1, total_de_tentativas + 1):
        chute = input("Digite o seu número entre 1 e 100: ") # Vem como str!
        chute = int(chute) # Converte o str para int!

        if (chute < 1 or chute > 100):
            print("Você deve digitar um número entre 1 e 100!")
            continue # Retorna pro início do for NO PRÓXIMO ÍNDICE DO CONTADOR!

        print("Você digitou", chute)

        acertou = chute == numero_secreto 
        maior = chute > numero_secreto
        menor = chute < numero_secreto

        if(acertou):
            print("Você acertou! Pontuação final: {}".format(pontos))
            break
        else:
            if (maior):
                pontos = pontos - (chute - numero_secreto)
                print("Tentativa {} de {}".format(rodada, total_de_tentativas))
                print("Você errou! O seu chute foi maior que o número secreto.")
            elif (menor):
                pontos = pontos - abs((chute - numero_secreto)) # abs() pega sempre o MÓDULO.
                print("Tentativa {} de {}".format(rodada, total_de_tentativas))
                print("Você errou! O seu chute foi menor que o número secreto.")
        # contador += 1

    print("Fim de jogo! O número secreto era: {}".format(numero_secreto))

    if(__name__ == "__main__"):
        jogar_adivinhacao()