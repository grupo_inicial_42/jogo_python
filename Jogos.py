#encoding: utf-8
import Forca
import Adivinhacao

def escolhe_jogo():
    print("*********************************")
    print("Bem vindo ao menu de jogos!")
    print("*********************************")

    print("(1) Forca // (2) Adivinhação")
    jogo = int(input("Qual jogo quer? "))

    if(jogo == 1):
        print("Iniciando jogo da Forca!")
        Forca.jogar_forca()
    elif(jogo == 2):
        print("Iniciando jogo da Adivinhação!")
        Adivinhacao.jogar_adivinhacao()

if (__name__ == "__main__"):
    escolhe_jogo()